// Laurine et Arnaud Champollion

// Éléments DOM
const conteneur=document.getElementById('conteneur');
const galerie=document.getElementById('galerie');
const bd=document.getElementById('bd');
const bouton_moins=document.getElementById('bouton_moins');
const bouton_plus=document.getElementById('bouton_plus');
const galeries=document.getElementById('galeries');
const galerie_images=document.getElementById('galerie_images');
const themes=document.getElementById('themes');
const bouton_supprimer=document.getElementById('bouton_supprimer');
const choix=document.getElementById('choix');
const police_choix=document.getElementById('police_choix');
const police_taille=document.getElementById('police_taille');
const police_plus=document.getElementById('police_plus');
const police_moins=document.getElementById('police_moins');
const bouton_annuler=document.getElementById('bouton_annuler');
const bouton_refaire=document.getElementById('bouton_refaire');
const bouton_cadenas=document.getElementById('bouton_cadenas');
const bouton_titre=document.getElementById('bouton_titre');
const titre_bd=document.getElementById('titre_bd');
const texte_a_propos=document.getElementById('texte_a_propos');
const dark=document.getElementById('dark');



// Surveillance du focus du titre
surveillerFocus(titre_bd);

// Surveillance de la hauteur du titre pour décaler les objets si nécessaire
let lastHeight = titre_bd.clientHeight;
const observer = new MutationObserver(() => {
  const newHeight = titre_bd.clientHeight;
  let correctif = 0;
  if (newHeight !== lastHeight) {
    if (lastHeight===0){correctif=20;}
    else if (newHeight===0){correctif=-20;}
    ajuste(newHeight+correctif-lastHeight);
    lastHeight = newHeight;
  }
});

const config = { attributes: true, childList: true, subtree: true };
observer.observe(titre_bd, config);
// Fin de la surveillance


let tailles = [
  6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 28, 30, 32, 34, 36, 38, 40, 42, 44, 46, 48, 50, 52, 54, 56, 58, 60, 62, 64, 66, 68, 70];

// Boucle pour ajouter chaque option au select
for (var i = 0; i < tailles.length; i++) {
  let option = document.createElement("option");
  option.text = tailles[i];
  option.classList.add('police');
  police_taille.add(option);
}

// Variables globales
redim=false;
dragged=null;
clicked=null;
paragraphe_focus=null;
hauteur=1;
bordureClic=5;
police_family='FuntypeRegular';
police_size=24;
vignette_background=null;
position=-1;
corry=0;
corrx=0;

// Listes
sauvegardes=[];
liste_bandes=[];
liste_vignettes=[];
liste_personnages=[];
liste_fonds=[];
liste_bulles=['bulle01','bulle02','bulle03','bulle04','bulle05','bulle06','bulle07','bulle08','bulle09','bulle10']

// Détections
posXbd=bd.offsetLeft;
posYbd=bd.offsetTop;

// Réglages de tailles
galerie_images.style.height=galerie.offsetHeight-choix.offsetHeight+'px';

// Création de la Liste des personnages
for (let i = 1; i <= 20; i++) {
    let numero = i.toString().padStart(2, '0'); // Pour avoir le format '01', '02', etc.
    liste_personnages.push('perso' + numero);
}
// Création de la liste des fonds
for (let i = 1; i <= 20; i++) {
  let numero = i.toString().padStart(2, '0'); // Pour avoir le format '01', '02', etc.
  liste_fonds.push(numero);
}

// Liste des fonds
liste_themes_fonds=[
    ['Montagne','Montagne'],
    ['École','École'],
    ['Restaurant','Restaurant'],
    ['Hôpital','Hôpital'],
    ['Lieux_publics','Lieux Publics'],
    ['Science_fiction','Science fiction'],
    ['Nature','Nature'],
    ['Neutre','Neutre'],
    ['Simples','Simples'],
    ['Plage','Plage'],
    ['Fantastique','Fantastique'],
    ['Dessins','Dessins'],
    ['Autres','Autres']
]

// Liste des poses
liste_themes_personnages=[
    ['Pose1','Position 1'],
    ['Pose2','Position 2'],
]

// Réglage des contrôles
police_taille.value=24;
bouton_annuler.disabled = true;
bouton_refaire.disabled = true;
lock=false;
change_liste_themes('Personnage1');
affiche('Personnage1','Pose1');
galeries.value='Personnage1';
themes.value='Pose1';
theme_fond='École';
police_choix.value=police_family;
cree_bande(3);
contenu_nouveau=bd.innerHTML;

// Début des fonctions

function change_lock() {
  if (lock){
    bouton_cadenas.style.backgroundPosition=null;
    document.querySelectorAll('.vignette, .bande').forEach(element => {
    element.classList.remove('hidden');
    });
  } else {
    bouton_cadenas.style.backgroundPositionY='0px';
    document.querySelectorAll('.vignette, .bande').forEach(element => {
    element.classList.add('hidden');
    });
  }
  lock=!lock;
}

function a_propos(){
  texte_a_propos.style.display='block';
  dark.style.display='block';

}

function quitter(){
  texte_a_propos.style.display=null;
  dark.style.display='none';

}

function change_titre() {
  if (titre_bd.style.display==='none'){
    titre_bd.style.fontFamily=police_family;
    titre_bd.style.fontSize=police_size+'px';
    titre_bd.style.display='block';
    let hauteurTitre = parseFloat(window.getComputedStyle(titre_bd).height);
    //ajuste(hauteurTitre+30)
  } else {
    let hauteurTitre = parseFloat(window.getComputedStyle(titre_bd).height);
    //ajuste(-hauteurTitre-30);
    titre_bd.style.display='none';
  }
}

function ajuste(hauteur) {
  // Sélectionnez tous les éléments ayant la classe 'draggable' dans la div avec l'ID 'bd'
  let elements = document.querySelectorAll('#bd .draggable');

  // Parcourez chaque élément et déplacez-le vers le bas de 30 pixels
  elements.forEach(function(element) {
      let currentPosition = window.getComputedStyle(element).getPropertyValue('top');
      let currentTop = parseInt(currentPosition) || 0;
      element.style.top = (currentTop + hauteur) + 'px';
  });
  }



function bubble(){
  let scrollBD = bd.scrollTop;
  let nouvelle_bulle = document.createElement('div');
  nouvelle_bulle.classList.add('bubble');
  nouvelle_bulle.classList.add('draggable');
  nouvelle_bulle.style.top = scrollBD+'px';
  nouvelle_bulle.style.setProperty('--margin-after', '-14px');
  nouvelle_bulle.style.setProperty('--margin-before', '-17px');
  createHandles(nouvelle_bulle);
  bd.appendChild(nouvelle_bulle);

  let nouvelle_span = document.createElement('span');
  nouvelle_bulle.appendChild(nouvelle_span);

  let nouveau_paragraphe = document.createElement('p');
  nouveau_paragraphe.contentEditable=true;
  nouveau_paragraphe.innerHTML='Texte';
  nouveau_paragraphe.style.fontFamily=police_family;
  nouveau_paragraphe.style.fontSize=police_size+'px';
  nouveau_paragraphe.style.zIndex=hauteur;
  hauteur+=1;
  surveillerFocus(nouveau_paragraphe);
  nouvelle_span.appendChild(nouveau_paragraphe);

  let nouvelle_pointe = document.createElement('div');
  nouvelle_pointe.classList.add('pointe');
  nouvelle_pointe.id=1;
  nouvelle_bulle.appendChild(nouvelle_pointe);

  //let nouvelle_image = document.createElement('img');
  //nouvelle_image.classList.add('image_pointe');
  //nouvelle_image.src='images/autres/pointe.svg';
  //nouvelle_pointe.appendChild(nouvelle_image);
}

// Affiche la galerie d'aperçus
function affiche(type,theme) {
    
    // Sélection de tous les éléments avec la classe "apercu"
    let elements = document.querySelectorAll('.apercu');

    // Parcourir chaque élément et le supprimer
    elements.forEach(function(element) {
        element.remove();
    });

    let liste;
    if (type==='Fonds'){liste=liste_fonds}
    else if (type==='Bulles'){liste=liste_bulles}
    else {liste=liste_personnages}
    liste.forEach(element => {
        let image = document.createElement('img');
        if (type==='Fonds'){
            image.src='images/'+type+'/'+theme+'/reduites/'+element+'.jpeg';
            image.classList.add('fond');
        }
        else if (type==='Bulles') {
            image.src='images/'+type+'/'+element+'.svg';
            image.classList.add('bulle');
        }
        else {
            image.src='images/'+type+'/'+theme+'/'+element+'.svg';
            image.classList.add('personnage');
        }

        image.classList.add('apercu');        
        image.draggable="true";
        image.onload = function() {
          galerie_images.appendChild(image);
        };


    });
}

// Création d'une vignette et de tous les boutons associés
function cree_vignette(mode,bande,cote_vignette,index_bande,index_vignette,reference) {
  let vignette = document.createElement('div');
  vignette.classList.add('vignette');  
  vignette.style.width=cote_vignette+'px';
  if (mode==='bande'){bande.appendChild(vignette);}
  else {
    if (mode===1){reference.insertAdjacentElement('afterend', vignette)}
    else {reference.insertAdjacentElement('beforebegin', vignette)}
  }

  let div_haut=document.createElement('div');
  div_haut.style.position='absolute';
  div_haut.style.top='0px';
  div_haut.style.width='100%';
  div_haut.style.textAlign='center';
  div_haut.classList.add('zone_outil','zone_haut');
  vignette.appendChild(div_haut);

  let nouvelle_div=document.createElement('div');
  nouvelle_div.classList.add('outils_vignette');
  nouvelle_div.classList.add('haut');
  div_haut.appendChild(nouvelle_div);

  let nouveau_bouton=document.createElement('button');
  nouveau_bouton.style.backgroundImage='url(images/boutons/zoom_in.svg)';
  nouveau_bouton.classList.add('bouton_vignette');
  nouveau_bouton.title="Zoomer l'image de fond";
  nouveau_bouton.addEventListener('click', function() {
    zoom(vignette,1);
  });
  nouvelle_div.appendChild(nouveau_bouton); 

  nouveau_bouton=document.createElement('button');
  nouveau_bouton.style.backgroundImage='url(images/boutons/zoom_out.svg)';
  nouveau_bouton.classList.add('bouton_vignette');
  nouveau_bouton.title="Dé-zéoomer l'image de fond";
  nouveau_bouton.addEventListener('click', function() {
    zoom(vignette,-1);
  });
  nouvelle_div.appendChild(nouveau_bouton); 

  nouveau_bouton=document.createElement('button');
  nouveau_bouton.style.backgroundImage='url(images/boutons/supprimer_background.png)';
  nouveau_bouton.classList.add('bouton_vignette');
  nouveau_bouton.title="Supprimer l'image de fond";
  nouveau_bouton.addEventListener('click', function() {
    supprime_fond(vignette);
  });
  nouvelle_div.appendChild(nouveau_bouton);

  let div_bas=document.createElement('div');
  div_bas.style.position='absolute';
  div_bas.style.bottom='0px';
  div_bas.style.width='100%';
  div_bas.style.textAlign='center';
  div_bas.classList.add('zone_outil','zone_bas');
  vignette.appendChild(div_bas);
  
  let nouvelle_div2=document.createElement('div');
  nouvelle_div2.classList.add('outils_vignette');
  nouvelle_div2.classList.add('bas');
  div_bas.appendChild(nouvelle_div2);

  nouveau_bouton=document.createElement('button');
  nouveau_bouton.style.backgroundImage='url(images/boutons/plus_gauche.svg)';
  nouveau_bouton.classList.add('bouton_vignette');
  nouveau_bouton.title="Ajouter une vignette à gauche";
  nouveau_bouton.addEventListener('click', function() {
    ajoute_vignette(vignette,0);
  });
  nouvelle_div2.appendChild(nouveau_bouton);

  nouveau_bouton=document.createElement('button');
  nouveau_bouton.style.backgroundImage='url(images/boutons/supprimer.svg)';
  nouveau_bouton.classList.add('bouton_vignette');
  nouveau_bouton.title="Supprimer cette vignette";
  nouveau_bouton.addEventListener('click', function() {
    supprime_vignette(vignette);
  });
  nouvelle_div2.appendChild(nouveau_bouton);

  nouveau_bouton=document.createElement('button');
  nouveau_bouton.style.backgroundImage='url(images/boutons/plus.svg)';
  nouveau_bouton.classList.add('bouton_vignette');
  nouveau_bouton.title="Ajouter une vignette à droite";
  nouveau_bouton.addEventListener('click', function() {
    ajoute_vignette(vignette,1);
  });
  nouvelle_div2.appendChild(nouveau_bouton);

  liste_vignettes[index_bande].splice(index_vignette, 0, vignette);
  vignette.id='vignette_'+index_bande+'_'+index_vignette;

}

// Création d'une nouvelle bande
function cree_bande(nb_vignettes){
    
  // Création d'une bande
  liste_vignettes.push([]);
  let bande = document.createElement('div');    
  bande.classList.add('bande');
  bd.appendChild(bande);
  liste_bandes.push(bande);
  let largeur_bande = largeur_vignette_droite=parseFloat(document.defaultView.getComputedStyle(bande).width, 10);
  let cote_vignette=largeur_bande/nb_vignettes-16;
  bande.style.height=cote_vignette+16+'px';
  let index_vignette = 0;

  // Création des vignettes
  for (let i = 0; i < nb_vignettes; i++) {
    cree_vignette('bande',bande,cote_vignette,liste_vignettes.length-1,index_vignette);        
    index_vignette+=1;
  }

  // Déplacement du bouton en bas
  bd.appendChild(bouton_moins);
  if(liste_bandes.length>1){bouton_moins.style.display='inline';}  
  bd.appendChild(bouton_plus);
  sauvegarde();
  
}

// Ajout manuel d'une nouvelle vignette
function ajoute_vignette(vignette,decalage){

  let largeur_vignette_a_decouper = parseFloat(document.defaultView.getComputedStyle(vignette).width, 10);
  let vignette_a_decouper=null;

  if (largeur_vignette_a_decouper>=76){
    vignette_a_decouper=vignette;
  }

  if (vignette_a_decouper) {
    vignette_a_decouper.style.width=largeur_vignette_a_decouper-76+'px';
    let index_bande=cherche_vignette(vignette)[0];
    let index_vignette=cherche_vignette(vignette)[1];
    cree_vignette(decalage,vignette.parentNode,60,index_bande,index_vignette+decalage,vignette);
  }

  sauvegarde();

}

// Récupération du ratio largeur / hauteur d'une vignette
function getRatio(vignette){
  let largeurVignette = vignette.offsetWidth-6;
  let hauteurVignette = vignette.offsetHeight-6;
  let ratio = largeurVignette/hauteurVignette;

  return ratio;
}

function getbackgroundSizeActuel(vignette){
  let style = window.getComputedStyle(vignette);
  let backgroundSizeString = style.getPropertyValue('background-size');

  if (backgroundSizeString==='cover'){
      
    if (getRatio(vignette)>=1) {
      backgroundSizeActuel=100;
    } else {
      backgroundSizeActuel=100/getRatio(vignette);
    }
  } else {  
    let valeurs = backgroundSizeString.split(' ');
    backgroundSizeActuel=parseFloat(valeurs[0]);
  }
  return backgroundSizeActuel;
}

function zoom(vignette,facteur){

  let backgroundSizeActuel=getbackgroundSizeActuel(vignette);
  let hauteurVignette=vignette.offsetHeight-6;
  let largeurVignette=vignette.offsetWidth-6;


  if (facteur>0){ // Zoom +
    vignette.style.backgroundSize=backgroundSizeActuel+3+'%';
  }

  else { // Zoom -

    if (backgroundSizeActuel-3<100) { // Éviter un background inférieur à 100%
      vignette.style.backgroundSize='100%';
    }

    else {
      let largeur_prevue=(backgroundSizeActuel-3)*largeurVignette/100;

      if (largeur_prevue>hauteurVignette) {  // Éviter un background inférieur à la hauteur
        vignette.style.backgroundSize=backgroundSizeActuel-3+'%';
      }

    }

  }

  sauvegarde();
}

// Suppression manuelle d'une vignette
function supprime_vignette(vignette){
  let index1=cherche_vignette(vignette)[0];
  let index2=cherche_vignette(vignette)[1];
  if (liste_vignettes[index1].length==1){
    supprime_bande(vignette.parentNode,index1);
  } else {
    let largeur_vignette_a_supprimer=parseFloat(document.defaultView.getComputedStyle(vignette).width, 10)+16;
    vignette.click=null;
    clicked=null;
    supprElementSurVignette(vignette);
    vignette.remove();
    let vignette_a_agrandir;

    if (index2===liste_vignettes[index1].length-1){
      vignette_a_agrandir=liste_vignettes[index1][index2-1];
    } else {
      vignette_a_agrandir=liste_vignettes[index1][index2+1];
    }
    let largeur_vignette_a_agrandir = parseFloat(document.defaultView.getComputedStyle(vignette_a_agrandir).width, 10);
    let nouvelle_largeur = largeur_vignette_a_agrandir + largeur_vignette_a_supprimer;
    vignette_a_agrandir.style.width=nouvelle_largeur+'px';            
  }

  // Suppression de la vignette dans la liste
  liste_vignettes[index1].splice(index2,1);

  sauvegarde();
 
}

// Recherche de l'index d'une vignette dans liste_vigettes (tableau à deux dimensions)
function cherche_vignette(vignette_a_chercher){  
  let index1;
  let index2;
  let num_bande=0;
  liste_vignettes.forEach(function (bande){
    let num_vignette=0;
    liste_vignettes[num_bande].forEach(function(vignette){
      if (vignette===vignette_a_chercher){index1=num_bande;index2=num_vignette;}
      num_vignette+=1;
    });
    num_bande+=1;
  });  
  return [index1,index2];
}


function getElementsSurVignette(vignette){
  const objets = document.querySelectorAll('.objet');
  // Obtenir les dimensions de la vignette
  const vignetteRect = vignette.getBoundingClientRect();
  // Créer une liste temporaire
  let liste_elementsSurVignette = [];  
  // Vérifier si les éléments en position absolute sont entièrement à l'intérieur de la vignette
  objets.forEach(element => {
    const elementRect = element.getBoundingClientRect();  
    // Vérifier si l'élément est entièrement à l'intérieur de la vignette
    if (
        elementRect.top >= vignetteRect.top &&
        elementRect.bottom <= vignetteRect.bottom &&
        elementRect.left >= vignetteRect.left &&
        elementRect.right <= vignetteRect.right
    ) {
        // Ajouter l'élément à une liste
        liste_elementsSurVignette.push(element);
    }
    });

  return liste_elementsSurVignette;
}

// Suppression des éléments de la vignette en cours
function supprElementSurVignette(vignette){
  const liste = getElementsSurVignette(vignette);
  console.log(liste)
  liste.forEach(element => {
    element.remove();
  });
} 


// Suppression manuelle d'une bande
function supprime_bande(bande,index){
    if (bande){
      bande.remove();
      liste_bandes.splice(index,1);
      liste_vignettes.splice(index,1);
    } else {
      liste_bandes[liste_bandes.length-1].remove();
      liste_bandes.pop();
      liste_vignettes.pop();
    }
    if (liste_bandes.length===0){bouton_moins.style.display=null;}
    if (bande){sauvegarde();}
}



// L'utilisateur a cliqué
function clic(event) {
    let cible = event.target;
    if (cible.parentNode.classList.contains('draggable') && !cible.classList.contains('handle')) {
        cible=cible.parentNode;
    }
    posXbd=bd.offsetLeft;
    posYbd=bd.offsetTop;

    declique('clic',cible);

    const borderWidth = 10; // Largeur de la bordure (en pixels)
    posX = event?.targetTouches?.[0]?.clientX || event.clientX;
    posY = event?.targetTouches?.[0]?.clientY || event.clientY;
    const rect = liste_bandes[liste_bandes.length-1].getBoundingClientRect();
    const mouseXrect =  posX - rect.left;
    const mouseYrect =  posY - rect.top;
    decalage_bd=bd.offsetLeft+5;
    decalage_bdy=bd.offsetTop+5;
    const scrollGalerie=galerie_images.scrollTop;
    const scrollBD=bd.scrollTop;


    if (cible.classList.contains('draggable')) {
        dragged = cible;
        clicked= cible;
        cible.click=true;
        event.preventDefault();
        vignette_en_cours=dragged.parentNode;
        cadre_ref=vignette_en_cours.getBoundingClientRect();

        diffsourisx = (posX - dragged.offsetLeft);
        diffsourisy = (posY - dragged.offsetTop);
        posX_objet = dragged.offsetLeft;
        posY_objet = dragged.offsetTop;
        dragged.classList.add('dragged');
        clicked.classList.add('clicked');
        cible.style.zIndex=hauteur;
        hauteur+=1;
        showHandles(clicked,null);      
    }

    else if (cible.classList.contains('personnage')) {
      event.preventDefault();
      diffsourisx = (posX - cible.offsetLeft);
      diffsourisy = (posY - cible.offsetTop + scrollGalerie);
      posX_objet = cible.offsetLeft;
      posY_objet = cible.offsetTop;

      let nouvel_objet = document.createElement('div');     
      nouvel_objet.classList.add('objet');
      nouvel_objet.classList.add('draggable');
      nouvel_objet.classList.add('temporaire');
      nouvel_objet.style.top=posY_objet-posYbd-scrollGalerie+scrollBD+'px';
      nouvel_objet.style.left=posX_objet-posXbd+'px';
      nouvel_objet.style.width=cible.offsetWidth+'px';
      nouvel_objet.style.height=cible.offsetHeight+'px';
      galerie.appendChild(nouvel_objet);
      
      let nouvelle_image = document.createElement('img');     
      nouvelle_image.src=cible.src;
      nouvelle_image.style.width=cible.offsetWidth+'px';
      nouvel_objet.appendChild(nouvelle_image);
      nouvel_objet.classList.add('dragged');
      nouvel_objet.style.zIndex=hauteur;
      hauteur+=1;
      dragged=nouvel_objet;
      corry=10;
      corrx=10;
      clicked=nouvel_objet;
      createHandles(dragged);
    }

    else if (cible.classList.contains('fond')) {
      event.preventDefault();
      diffsourisx = (posX - cible.offsetLeft);
      diffsourisy = (posY - cible.offsetTop + scrollGalerie);
      posX_objet = cible.offsetLeft;
      posY_objet = cible.offsetTop;
      let nouvel_objet = document.createElement('div');     
      nouvel_objet.classList.add('objet');
      nouvel_objet.classList.add('fond');
      nouvel_objet.classList.add('draggable');
      nouvel_objet.classList.add('temporaire');
      nouvel_objet.style.top=posY_objet-posYbd-scrollGalerie+scrollBD+'px';
      nouvel_objet.style.left=posX_objet-posXbd+'px';
      nouvel_objet.style.width=cible.offsetWidth+'px';
      nouvel_objet.style.height=cible.offsetHeight+'px';
      nouvel_objet.source=cible.src;

      galerie.appendChild(nouvel_objet);
      
      let nouvelle_image = document.createElement('img');     
      nouvelle_image.src=cible.src;
      nouvelle_image.style.width=cible.offsetWidth+'px';
      nouvelle_image.style.height=cible.offsetHeight+'px';
      nouvel_objet.appendChild(nouvelle_image);
      nouvel_objet.classList.add('dragged');
      nouvel_objet.style.zIndex=hauteur;
      hauteur+=1;
      corry=10;
      corrx=10;
      dragged=nouvel_objet;
      clicked=nouvel_objet;
    }
    
    else if (cible.classList.contains('handle')) {
        let resizingHandle = cible;
        let resizableElement = resizingHandle.parentNode;
        let image_a_redim = resizableElement.querySelector("img");       
        let startX = event.clientX;
        let startY = event.clientY;
        let bulle = resizableElement.classList.contains('bubble') || resizableElement.classList.contains('bubble');
        let inverse = resizableElement.classList.contains('inverse');
        // Récupération des valeurs de départ au moment du clic
        if (image_a_redim){
          startTopImage = parseFloat(document.defaultView.getComputedStyle(image_a_redim).top, 10);
          startLeftImage = parseFloat(document.defaultView.getComputedStyle(image_a_redim).left, 10);
          startWidthImage = parseFloat(document.defaultView.getComputedStyle(image_a_redim).width, 10);
          startHeightImage = parseFloat(document.defaultView.getComputedStyle(image_a_redim).height, 10);
        }
        let startWidthResizable = parseFloat(document.defaultView.getComputedStyle(resizableElement).width, 10);
        let startHeightResizable = parseFloat(document.defaultView.getComputedStyle(resizableElement).height, 10);
        let startTopResizable = parseFloat(document.defaultView.getComputedStyle(resizableElement).top, 10)+decalage_bdy;
        let startLeftResizible = parseFloat(document.defaultView.getComputedStyle(resizableElement).left, 10)+decalage_bd;
        let pointe=null;
        if (bulle){
          pointe = resizableElement.querySelector('.pointe');
          fleche = resizableElement.querySelector('.fleche');
          startLeftPointe = parseFloat(document.defaultView.getComputedStyle(pointe).left, 10);
          startLeftPoignee = parseFloat(document.defaultView.getComputedStyle(resizingHandle).left, 10);
          startBottomPoignee = parseFloat(document.defaultView.getComputedStyle(resizingHandle).bottom, 10);
          oldScaleY = parseFloat(pointe.id);
          minLeftPointe = 15;
          maxLeftPointe = startWidthResizable - 45;
        }
        
        document.addEventListener('mousemove', resizeElement);
        document.addEventListener('mouseup', stopResize);
        
        // Début du redimensionnement
        function resizeElement(event) {

            // Position de la souris
            let mouseX = event.clientX;
            let mouseY = event.clientY;
            
            // Déplacement de la souris depuis la position de départ
            let diffX = mouseX - startX;
            let diffY = mouseY - startY;
            
            // Fonction de déplacement de la flèche de la bulle

            if (resizingHandle.classList.contains('fleche')) { 
              if (inverse){diffX*=-1;}             
              let newLeftPointe = startLeftPointe + diffX;
              // Empêche de dépasser
              if (newLeftPointe<minLeftPointe){newLeftPointe=minLeftPointe} 
              else if (newLeftPointe>maxLeftPointe){newLeftPointe=maxLeftPointe}  

              pointe.style.left = newLeftPointe + 'px';
              resizingHandle.style.left = newLeftPointe + 'px';

              let newScaleY=oldScaleY+diffY/35;
              let newBottom=startBottomPoignee - diffY;
              if (newScaleY<0){newScaleY=0;newBottom=0;}
              pointe.style.transform = 'scaleY('+newScaleY+')';
              resizingHandle.style.bottom = newBottom + 'px';
              pointe.id=newScaleY;
              

            }
            // Fonctions "rogner"
            
            else if (resizingHandle.classList.contains('top')) { // Haut
              let newHeight = startHeightResizable - diffY;
              if (newHeight<=startHeightImage){ // Empêche un rognage négatif
                resizableElement.style.height = newHeight + 'px';
                resizableElement.style.top = startTopResizable + diffY - decalage_bdy + 'px';
                if (image_a_redim){image_a_redim.style.top=startTopImage-diffY+'px';}
              }

            } else if (resizingHandle.classList.contains('bottom')) { // Bas
              let newHeight = startHeightResizable + diffY;
              if (newHeight<=startHeightImage){ // Empêche un rognage négatif
                resizableElement.style.height = newHeight + 'px';
              }            
            
            } else if (resizingHandle.classList.contains('left')) { // Gauche
              let newWidth = startWidthResizable - diffX;
              if ((bulle && newWidth>=140) || (!bulle && newWidth<=startWidthImage)){ // Empêche un rognage négatif
                resizableElement.style.width = newWidth + 'px';
                resizableElement.style.left = startLeftResizible + diffX - decalage_bd+ 'px';
                if (bulle){
                  if (inverse){
                    if (bulle && diffX >0 && startLeftPointe > newWidth - 45){
                      let newLeftPointe = newWidth - 45;                  
                      pointe.style.left = newLeftPointe + 'px';
                      fleche.style.left = newLeftPointe + 'px';
                    }
                  } else {
                    let newLeftPointe = startLeftPointe - diffX;
                    if (newLeftPointe<minLeftPointe){newLeftPointe=minLeftPointe;}                  
                    pointe.style.left = newLeftPointe + 'px';
                    fleche.style.left = newLeftPointe + 'px';
                  }                  
                }
                if (image_a_redim){image_a_redim.style.left=startLeftImage-diffX+'px';}
              }

            } else if (resizingHandle.classList.contains('right')) { // Droite
              let newWidth = startWidthResizable + diffX;

              if ((bulle && newWidth>=140) || (!bulle && newWidth<=startWidthImage)) { // Empêche un rognage négatif
                let newWidth = startWidthResizable + diffX;
                resizableElement.style.width = newWidth + 'px';

                  if (inverse){ // Si miroir
                    let newLeftPointe = startLeftPointe + diffX;
                    if (newLeftPointe<minLeftPointe){newLeftPointe=minLeftPointe;}                  
                    pointe.style.left = newLeftPointe + 'px';
                    fleche.style.left = newLeftPointe + 'px'; 

                  } else { // Si pas miroir

                    if (bulle && diffX <0 && startLeftPointe + 10 > newWidth - 35){
                      let newLeftPointe = newWidth - 45;                  
                      pointe.style.left = newLeftPointe + 'px';
                      fleche.style.left = newLeftPointe + 'px';
                    }
                  }
              }
            

            // Fonctions "redimensionner"

            }  else if (resizingHandle.classList.contains('top-left')) {  // Haut gauche
              let diffXY = (diffX + diffY) / 4;
              let newWidth;
              newWidth = startWidthResizable - diffXY;
              resizableElement.style.width = newWidth + 'px';
              let newHeight = proportions(newWidth);
              let diffHeight = newHeight - startHeightResizable;
              resizableElement.style.left = startLeftResizible + diffXY - decalage_bd + 'px';
              resizableElement.style.top = startTopResizable - diffHeight - decalage_bdy + 'px';             


            } else if (resizingHandle.classList.contains('top-right')) { // Haut droite
              let diffXY = (diffX - diffY) / 4;
              let newWidth;
              newWidth = startWidthResizable + diffXY;
              resizableElement.style.width = newWidth + 'px';             
              let newHeight=proportions(newWidth);
              let diffHeight = newHeight - startHeightResizable;
              resizableElement.style.top = startTopResizable - diffHeight - decalage_bdy + 'px';



            } else if (resizingHandle.classList.contains('bottom-left')) { // Bas gauche
              let diffXY = (diffX - diffY) / 4;
              let newWidth;
              newWidth = startWidthResizable - diffXY;
              resizableElement.style.width = newWidth + 'px';
              proportions(newWidth);
              resizableElement.style.left = startLeftResizible + diffXY - decalage_bd + 'px';



            } else if (resizingHandle.classList.contains('bottom-right')) { // bas droite
              let diffXY = (diffX + diffY) / 4;
              let newWidth;
              newWidth = startWidthResizable + diffXY;
              resizableElement.style.width = newWidth + 'px';
              proportions(newWidth);
            }
            
            function proportions(newWidth){   
              let agrandissement = newWidth / startWidthResizable;
              let newHeight = startHeightResizable * agrandissement;
              resizableElement.style.height = newHeight +'px';
               
              image_a_redim.style.width=startWidthImage*agrandissement+'px';
              image_a_redim.style.top=startTopImage*agrandissement+'px';
              image_a_redim.style.left=startLeftImage*agrandissement+'px';

              return newHeight;
            }


          }
          
          
          
    
        function stopResize() {
          document.removeEventListener('mousemove', resizeElement);
          document.removeEventListener('mouseup', stopResize);
          sauvegarde();
        }
    }

    
    else { 

      if (cible.classList.contains('vignette')){
        clicked=cible;
        clicked.style.borderColor='red';
        redim="background";
        vignette_background=cible;
        cible.classList.add('vignette_selectionnee');
        let styles = window.getComputedStyle(cible);
        backgroundPositionXdepart = extractNumericValue(styles.getPropertyValue("background-position-x"));
        backgroundPositionYdepart = extractNumericValue(styles.getPropertyValue("background-position-y"));
        posXDepartSouris=posX;
        posYDepartSouris=posY;      
      }
      if (!lock) {

      // Seulement si cadenas ouvert

        // Redimensionnement de la dernière bande
        if (mouseYrect <= rect.height+borderWidth && mouseYrect>rect.height - borderWidth && mouseXrect>=0) {
            bande=liste_bandes[liste_bandes.length-1];
            redim='fin';
            posYDepartSouris=posY;
            hauteur_bande=rect.height;
        }

        // Redimensionnement entre bandes
        if (liste_bandes.length>1){
          for (let k = 1; k < liste_bandes.length; k++) {
              const rect = liste_bandes[k].getBoundingClientRect();
              const mouseXrect =  posX - rect.left;
              const mouseYrect =  posY - rect.top;
          
              if (mouseYrect <= borderWidth && mouseYrect>-borderWidth && mouseXrect>=0){
                  redim='v';
                  bande_haut=liste_bandes[k-1];
                  bande_bas=liste_bandes[k];
                  posYDepartSouris=posY;
                  hauteur_bande_haut=bande_haut.offsetHeight;
                  hauteur_bande_bas=bande_bas.offsetHeight;
              }
          }
        }

        // Redimensionnement entre vignettes
        for (let i = 0; i < liste_vignettes.length; i++) {
          for (let j = 1; j < liste_vignettes[i].length; j++) {
              const rect = liste_vignettes[i][j].getBoundingClientRect();
              const mouseXrect =  posX - rect.left;
              const mouseYrect =  posY - rect.top;
          
              if ((mouseXrect <= borderWidth && mouseXrect>-borderWidth)&&(mouseYrect >= 0 && mouseYrect < rect.height)){
                  redim='h';
                  vignette_droite=liste_vignettes[i][j];
                  vignette_gauche=liste_vignettes[i][j-1];
                  posXDepartSouris=posX;
                  largeur_vignette_droite=parseFloat(document.defaultView.getComputedStyle(vignette_droite).width, 10);
                  largeur_vignette_gauche=parseFloat(document.defaultView.getComputedStyle(vignette_gauche).width, 10);
              }
          }
        }

      } // Fin des fonctions liés au cadenas

      else {
        
      }


    }


}

// Fonction pour convertir les valeurs en pixels
function convertToPixels (value)  {
  // Convertit la valeur en pixels en utilisant parseFloat
  return parseFloat(value);
};

function extractNumericValue(value) {
  // Utilisation d'une expression régulière pour extraire les nombres d'une chaîne
  var numericValue = parseFloat(value);
  return isNaN(numericValue) ? 0 : numericValue;
}

function createHandles(element) {

  let tour=1;

  const handles = [
    'top-left', 'top-right', 'bottom-left', 'bottom-right', 'top', 'bottom', 'left', 'right','fleche'
  ];
  
  handles.forEach(handleClass => {
    const handle = document.createElement('div');
    handle.classList.add('handle', handleClass);
 
    if (tour<5){handle.classList.add('coin');}    
 
    element.appendChild(handle);
    tour+=1;

  });


}


function move(event) {

    posXSouris=event?.targetTouches?.[0]?.clientX || event?.clientX;
    posYSouris=event?.targetTouches?.[0]?.clientY || event?.clientY;

    if (redim==='h'){
        diffX = posXSouris - posXDepartSouris;
        if((largeur_vignette_droite - diffX) >= 60 && (largeur_vignette_gauche + diffX) >= 60){
          vignette_droite.style.width=largeur_vignette_droite - diffX + 'px';
          if (getbackgroundSizeActuel(vignette_droite)/100>getRatio(vignette_droite)){
            vignette_droite.style.backgroundSize='cover';
          }
          vignette_gauche.style.width=largeur_vignette_gauche + diffX + 'px';
          if (getbackgroundSizeActuel(vignette_gauche)/100>getRatio(vignette_gauche)){
            vignette_gauche.style.backgroundSize='cover';
          }
        } 
    } else if (redim==='v'){
        diffY = posYSouris - posYDepartSouris;
        if((hauteur_bande_haut + diffY - 7) >= 40){
          bande_haut.style.height=hauteur_bande_haut + diffY - 7 + 'px';
        } else {
          bande_haut.style.height=40 + 'px';
        }
    } else if (redim==='fin'){
        diffY = posYSouris - posYDepartSouris;
        if((hauteur_bande + diffY - 7) >= 40){
          bande.style.height=hauteur_bande + diffY + 'px';
        } else {
          bande.style.height=40 + 'px';
        }
    }  else if (redim==='background') {

      diffX = posXSouris - posXDepartSouris;
      diffY = posYSouris - posYDepartSouris;

      let nouvellePositionX=backgroundPositionXdepart-diffX;
      let nouvellePositionY=backgroundPositionYdepart-diffY;

      if (nouvellePositionX<0){
        vignette_background.style.backgroundPositionX='0%';
      } else if (nouvellePositionX>100){
        vignette_background.style.backgroundPositionX='100%';
      } else {        
        vignette_background.style.backgroundPositionX=nouvellePositionX+'%';
      }

      if (nouvellePositionY<0){
        vignette_background.style.backgroundPositionY='0%';
      } else if (nouvellePositionY>100){
        vignette_background.style.backgroundPositionY='100%';
      } else {
        vignette_background.style.backgroundPositionY=nouvellePositionY+'%';
      }

    }

    if (dragged) {        
        event.preventDefault();
        const pageX = event?.targetTouches?.[0]?.clientX || event?.clientX || 0;
        const pageY = event?.targetTouches?.[0]?.clientY || event?.clientY || 0;
        const scrollBD = bd.scrollTop;
        opacite(dragged,0.3);
        dragged.style.left = pageX - diffsourisx  - corrx + "px";
        dragged.style.top = pageY - diffsourisy - corry + "px";
    }

}

function opacite(objet,valeur){
  // Sélectionne tous les éléments ayant la classe "handle"
const elements = document.querySelectorAll('.handle');

// Parcourt chaque élément et affiche ses enfants
elements.forEach(element => {
  element.style.opacity=valeur;
});
}

function verifieVignette() {

  // Obtient les coordonnées de la souris
  const x = event?.targetTouches?.[0]?.clientX || event?.clientX || 0;;
  const y = event?.targetTouches?.[0]?.clientY || event?.clientY || 0;

  // Récupère l'élément sous la position actuelle de la souris
  const elementsSousSouris = document.elementsFromPoint(x, y);

  // Filtrer la première div avec la classe "vignette"
  let premiereDivVignette = null;
  for (let i = 0; i < elementsSousSouris.length; i++) {
      if (elementsSousSouris[i].classList.contains('vignette')) {
          premiereDivVignette = elementsSousSouris[i];
          break;
      }
  }

  return premiereDivVignette;
}


function release(event) {
    if (redim){sauvegarde();}
    redim=false;

    if (dragged) {
      opacite(dragged,1);
      let scrollBD=bd.scrollTop;
      if (dragged.classList.contains('fond')) {
        if (dragged.classList.contains('temporaire')){
          dragged.classList.remove('temporaire');
          let topValue = parseFloat(getComputedStyle(dragged).top)-decalage_bdy+scrollBD+5;
          let leftValue = parseFloat(getComputedStyle(dragged).left)-decalage_bd+5;        
          corrx=0;
          corry=0;
          if ( topValue<-50 || leftValue<-50 ){
            dragged.remove();
          } else {
            bd.appendChild(dragged);
            dragged.style.top=topValue+'px';
            dragged.style.left=leftValue+'px';
          }
        }
        let vignette = verifieVignette();
        if (vignette){
          vignette.style.backgroundImage='url('+(dragged.source).replace(/\/reduites/g, '')+')';
          vignette.style.cursor='all-scroll';
          clicked=vignette;
          clicked.style.borderColor='red';
          clicked.classList.add('background');
          clicked.classList.add('vignette_selectionnee');
          dragged.remove();
          dragged=null;          
        } else {
          dragged.classList.remove('dragged');      
          dragged=null;
        }
        
      } else {
        dragged.classList.remove('dragged');
        if (dragged.classList.contains('temporaire')){
          dragged.classList.remove('temporaire');
          let topValue = parseFloat(getComputedStyle(dragged).top)-decalage_bdy+scrollBD+5;
          let leftValue = parseFloat(getComputedStyle(dragged).left)-decalage_bd+5;          
           
          if ( topValue<-50 || leftValue<-50 ){
            dragged.remove();
          } else {
            bd.appendChild(dragged);
            dragged.style.top=topValue+'px';
            dragged.style.left=leftValue+'px';
            dragged=null;
          }
          corrx=0;
          corry=0;
        } else {
          dragged=null;
        }
      }
      sauvegarde();      
    } 

}

// Duplication d'un objet
function duplique(){
  if (clicked && (clicked.classList.contains('objet') || clicked.classList.contains('bubble'))){    
    const objet=clicked;    
    const divDupliquee = objet.cloneNode(true);
    declique('clic',divDupliquee);
    clicked=divDupliquee;
    const styles = window.getComputedStyle(objet);
    const posXobjet = parseFloat(styles.left);
    const posYobjet = parseFloat(styles.top); 
    divDupliquee.style.left=posXobjet+30+'px';
    divDupliquee.style.top=posYobjet+30+'px';
    bd.appendChild(divDupliquee);
    sauvegarde();
  }
}

function sauvegarde(){
  // Supression éventuelle des sauvegardes inutiles
  if (position<sauvegardes.length-1){
    sauvegardes.splice(position+1)
  }

  // Sauvegarde du contenu de la BD
  const sauvegarde = bd.innerHTML;
  sauvegardes.push(sauvegarde);

  // On avance d'une position
  position+=1;

  // On met à jour les boutons annuler / refaire
  update_boutons();
}

function annuler(){
  if (position>0){ // Empêche d'annuler en-deça de la position 0
    // On recule d'une position
    position-=1;
    // Restauration du contenu de la BD
    bd.innerHTML=sauvegardes[position];
    
    // Reconstitution des listes de bandes et vignettes
    restaure_listes();    
    raz_globales();
    update_boutons();

  }
}

function refaire(){
  if (position<sauvegardes.length-1){ // Empêche de restaurer plus loin que la liste des positions
    // On avance d'une position
    position+=1;
    // Restauration du contenu de la BD
    bd.innerHTML=sauvegardes[position];
    // Reconstitution des listes de bandes et vignettes
    restaure_listes();
    raz_globales();
    update_boutons();

  }
}

function update_boutons(){

  if (position<1){bouton_annuler.disabled=true;}
  else {bouton_annuler.disabled=false;}

  if (position===sauvegardes.length-1){bouton_refaire.disabled=true;}
  else {bouton_refaire.disabled=false;}
}

function raz_globales(){

    // On récupère l'objet cliqué s'il y en a un
    let elements = document.querySelectorAll('.vignette, .objet');
    let objet_a_recuperer=null;
    elements.forEach(element => {
        if (element.click){objet_a_recuperer=element}
    });
    if (objet_a_recuperer){clicked=objet_a_recuperer;clicked.click=true;}

    // On récupère le focus sur le paragraphe s'il y en a un
    elements = document.querySelectorAll('p');
    let paragraphe_a_recuperer=null;
    elements.forEach(element => {
      if (element.focus){paragraphe_a_recuperer=element}
    });
    if (paragraphe_a_recuperer){paragraphe_focus=paragraphe_a_recuperer;paragraphe_focus.focus=true;}

    // On réatttibue les évènements onclick
    elements = document.querySelectorAll('.vignette');
    elements.forEach(element => {
      const tousLesBoutons = element.querySelectorAll('button');
      const liste_fonctions = [
        { fonction: zoom, arguments: [element,1] },
        { fonction: zoom, arguments: [element,-1] },
        { fonction: supprime_fond, arguments: [element] },
        { fonction: ajoute_vignette, arguments:[element,0] },
        { fonction: supprime_vignette, arguments: [element] },
        { fonction: ajoute_vignette, arguments: [element,0] }
    ];
    
      // Attribution des fonctions aux boutons
      tousLesBoutons.forEach((button, index) => {
       button.addEventListener("click", function() {
        liste_fonctions[index].fonction.apply(null, liste_fonctions[index].arguments);
        });
      });

    

    });
}

function restaure_listes(){

    // Sélection de tous les éléments div avec la classe 'bande'
    let divsBande = document.querySelectorAll('div.bande');
    // Remise à zéro de la liste des bandes
    liste_bandes = [];
    // Copie des éléments dans la liste
    divsBande.forEach(function(div) {
      liste_bandes.push(div);
    });

     // Remise à zéro de la liste des vignettes
    liste_vignettes = [];
    // Parcours de la liste des bandes
    liste_bandes.forEach(function(bande) {
    // Sélection des div enfants avec la classe 'vignette' pour la bande actuelle
    let vignettes = bande.querySelectorAll('.vignette');    
    // Stockage des vignettes de la bande actuelle dans une liste temporaire
    let tableauVignettes = [];
    vignettes.forEach(function(vignette) {
          tableauVignettes.push(vignette);
      });    
      // Ajout du tableau de vignettes de la bande actuelle au tableau liste_vignettes
      liste_vignettes.push(tableauVignettes);
    });
}

function showHandles(objet,valeur) {
  const enfantsAvecClasseHandle = objet.getElementsByClassName('handle');
      const enfantsArray = Array.from(enfantsAvecClasseHandle);
      enfantsArray.forEach(function(enfant) {
        enfant.style.display=valeur;
      });
}

 

function declique(mode,cible){
  if (mode==='clic'){
    if (clicked && !cible.classList.contains('bouton_outil') && !cible.classList.contains('handle') && !cible.classList.contains('police') && !cible.classList.contains('bouton_vignette')){
      showHandles(clicked,'none');
      clicked.style.borderColor=null;
      clicked.classList.remove('vignette_selectionnee');
      clicked.click=null;
      clicked=null;
      if (!cible.classList.contains('bouton_outil') && paragraphe_focus){paragraphe_focus.focus=null;paragraphe_focus=null;}
    }
  }
  else if (clicked){
    clicked.style.boxShadow=null;
    clicked.click=null;
    clicked=null;
  }
  
}

async function creerImage() {

  const div = document.getElementById('bd');

  // Utilisation de html2canvas pour capturer le rendu de la div
  bouton_moins.style.display="none";
  bouton_plus.style.display="none";
  bd.style.height='auto';
  if (clicked){declique();}
  const canvas = await html2canvas(div);
  bouton_moins.style.display=null;
  bouton_plus.style.display=null;
  bd.style.height=null;

  // Convertir le canvas en une URL d'image
  const image = canvas.toDataURL('image/png');

  // Créer un lien de téléchargement pour l'image
  const lienTelechargement = document.createElement('a');
  lienTelechargement.href = image;
  lienTelechargement.download = 'ma_bd.png';

  // Simuler le clic sur le lien pour déclencher le téléchargement
  document.body.appendChild(lienTelechargement);
  lienTelechargement.click();
  document.body.removeChild(lienTelechargement);
}



function enregistre() {

  var contenu = bd.innerHTML;

  var fichier = new Blob([contenu], { type: 'text/plain' });
  var lien = document.createElement('a');
  lien.href = URL.createObjectURL(fichier);

  // Définir un nom de fichier par défaut (peut être vide ici)
  lien.setAttribute('download', 'bd_sauvegardee.txt');

  lien.click();

}

function ouvre(){
  var input = document.getElementById('choisirFichier');

  input.addEventListener('change', function() {
    var file = input.files[0];
    var reader = new FileReader();

    reader.onload = function(event) {
      var contenu = event.target.result;
      bd.innerHTML = contenu;
    };

    reader.readAsText(file);
  });

  // Déclenche le sélecteur de fichier
  input.click();
}

function change_liste_themes(option){
    let liste;
    if (option==='Fonds'){liste=liste_themes_fonds;}
    else {liste=liste_themes_personnages}
    themes.innerHTML='';
    liste.forEach(function(element){
        let option=document.createElement('option');
        option.value=element[0];
        option.innerHTML=element[1];
        themes.appendChild(option);
    });
}


function nouveau(){
    bd.innerHTML=contenu_nouveau;
    // Reconstitution des listes de bandes et vignettes
    restaure_listes();    
    raz_globales();
    update_boutons();
    // Sauvegarde
    sauvegarde();
}

function supprime_fond(vignette){
  let largeur_fond=getbackgroundSizeActuel(vignette)/100;
  vignette.style.transition='background-position 0.8s linear';  
  vignette.style.backgroundPosition='-'+largeur_fond*1024+'px'+' -'+largeur_fond*1024+'px';
  setTimeout(function() {
    vignette.style.transition=null;
    vignette.style.backgroundImage=null;
    vignette.classList.remove('background');
    vignette.style.backgroundSize=null;
    vignette.style.backgroundPosition=null;
  }, 800);
  sauvegarde();  
}

function supprime(mode){
  if (clicked){
    if (clicked.classList.contains('vignette')){
      supprime_vignette(clicked);
    } else {
    clicked.remove();
    }
  } else if (paragraphe_focus && mode!='clavier') {
    paragraphe_focus.parentNode.parentNode.remove();
    paragraphe_focus.focus=null;
    paragraphe_focus=null;
  }
  sauvegarde();
}

function miroir(){
  if (clicked || paragraphe_focus){
    let objet_a_retourner;
    if (clicked) {objet_a_retourner = clicked;}
    if (paragraphe_focus){objet_a_retourner = paragraphe_focus.parentNode.parentNode;}

    let image = objet_a_retourner.querySelector("img");

    if (image){
      if (image.style.transform !='scaleX(-1)'){image.style.transform='scaleX(-1)';}
      else {image.style.transform=null;}
    }

    else {

      if (objet_a_retourner.classList.contains('bubble')){
        let enfant=objet_a_retourner.querySelector('span').querySelector('p');
        if (enfant.style.transform !='scaleX(-1)'){
          enfant.style.transform='scaleX(-1)';      
        }
        else {enfant.style.transform=null;}
      }

      if (objet_a_retourner.style.transform !='scaleX(-1)'){
        objet_a_retourner.style.transform='scaleX(-1)';
        objet_a_retourner.classList.add('inverse');      
      }
      else {
        objet_a_retourner.style.transform=null;
        objet_a_retourner.classList.remove('inverse');      
      }

    }
    sauvegarde();
  }
}

function back(){
  if (clicked){
    clicked.style.zIndex=0;
    sauvegarde();
  }
}

function front(){
  if (clicked){
    clicked.style.zIndex=hauteur;
    hauteur+=1;
    sauvegarde();
  }
}

function police (facteur,taille){

  if (taille){police_size=taille;}
  else if (facteur !=0){police_size+=facteur;}

  let paragraphe=null;

  if (clicked){
    paragraphe=clicked.firstElementChild.firstElementChild;
    if (paragraphe.tagName.toLowerCase() != 'p') {paragraphe=null;}
  }
  else if (paragraphe_focus){paragraphe=paragraphe_focus;}

  if (paragraphe){
    let style_paragraphe=window.getComputedStyle(paragraphe);
    let fontSize = style_paragraphe.getPropertyValue('font-size');
    let currentFontSize = parseFloat(fontSize);
    if (!taille){police_size=currentFontSize+facteur;}
    paragraphe.style.fontSize=police_size+'px';    
    sauvegarde();
  }
  police_taille.value=police_size;
}

function deselectionnerTexte() {
  if (window.getSelection) {
      window.getSelection().removeAllRanges();
  } else if (document.selection) {
      // Pour les anciennes versions d'Internet Explorer
      document.selection.empty();
  }
}


function surveillerFocus(editable) {
  editable.addEventListener('focus', function() {
    paragraphe_focus=editable;
    paragraphe_focus.focus=true;
  });
  editable.addEventListener('blur', function() {
   deselectionnerTexte();
  });
}


function update_taille(paragraphe){
  let bulle = paragraphe.parentNode.parentNode;
  let conteneur_bulle = paragraphe.parentNode;
  let top = 0.1;
  let bottom = 0.5;
  let left = 0.1;
  let right = 0.1;
  if (bulle.style.backgroundImage==='url("images/Bulles/bulle02.svg")') {
    top = 0.5;
    bottom = 0.7;
  } else if (bulle.style.backgroundImage==='url("images/Bulles/bulle03.svg")') {
    top = 0.5;
    bottom = 0.1;
    left=0.2;
    right=0.2;
  }  else if (bulle.style.backgroundImage==='url("images/Bulles/bulle05.svg")') {
    left=0.03;
    right=0.03;
  }  else if (bulle.style.backgroundImage==='url("images/Bulles/bulle06.svg")' || bulle.style.backgroundImage==='url("images/Bulles/bulle07.svg")') {
    top = 0.2;
    bottom = 0.6;
    left=0.01;
    right=0.01;
  } else if (bulle.style.backgroundImage==='url("images/Bulles/bulle08.svg")') {
    left=0.01;
    right=0.01;
  }
  bulle.style.paddingTop=null;
  bulle.style.paddingBottom=null; 
  conteneur_bulle.style.height=null;
  hauteurBulle = bulle.offsetHeight;
  largeurBulle = bulle.offsetWidth;
  bulle.style.paddingTop=hauteurBulle*top+'px';
  bulle.style.paddingBottom=hauteurBulle*bottom+'px';
  bulle.style.paddingLeft=largeurBulle*left+'px';
  bulle.style.paddingRight=largeurBulle*right+'px';
  conteneur_bulle.style.height=hauteurBulle+'px';
  sauvegarde();
}

function change_police(valeur){
  police_family=valeur;
  police_choix.style.fontFamily=valeur;
  let paragraphe=null;
  if (clicked){
    paragraphe=clicked.firstElementChild.firstElementChild;
    if (paragraphe.tagName.toLowerCase() != 'p') {paragraphe=null;}
  }
  else if (paragraphe_focus){paragraphe=paragraphe_focus;}

  if (paragraphe){
    paragraphe.style.fontFamily=police_family;
    sauvegarde();
  }
}

// Écouteurs de souris
document.addEventListener("touchstart", clic);
document.addEventListener("touchmove", move);
document.addEventListener("touchend", release);
// Pour le tactile
document.addEventListener("mousedown", clic);
document.addEventListener("mousemove", move);
document.addEventListener("mouseup", release);

function active_bulles(){
  galeries.value='Bulles';
  updateGaleries('Bulles');
}

function updateGaleries(optionSelectionnee) {
  if (optionSelectionnee==='Bulles' || optionSelectionnee==='Jous' ||  optionSelectionnee==='Autres_personnages' ){themes.style.display='none';}
  else {themes.style.display=null}
  change_liste_themes(optionSelectionnee);
  if (optionSelectionnee==='Fonds'){themes.value=theme_fond;}
  affiche(optionSelectionnee,themes.value);
}


galeries.addEventListener('change', (e) => {
  updateGaleries(e.target.value);

});

themes.addEventListener('change', (e) => {
    const optionSelectionnee = e.target.value;
    if (galeries.value==='Fonds'){theme_fond=optionSelectionnee;}
    affiche(galeries.value,optionSelectionnee);
});


document.addEventListener('keydown', function(event) {

  let capsLockOn = event.getModifierState('CapsLock');

  if (event.key === 'Delete') {
      supprime('clavier');
  }

  if ((event.ctrlKey && event.key === 'z') || (capsLockOn && event.key === 'Z')) {
    annuler();
  }

  if ((event.ctrlKey && event.key === 'y') || (capsLockOn && event.key === 'Y')) {
    refaire();
  }
  });
  

  function surveillerFocus(editable) {
    editable.addEventListener('focus', function() {
      paragraphe_focus=editable;
      paragraphe_focus.focus=true;
    });
    editable.addEventListener('blur', function() {
      deselectionnerTexte();
    });
  }
  